import React from 'react';
import './Input.css';

const Input = ({ attribute, value, onChange, onBlur}) => {
    return(
        <div>
            <input
            id={attribute.id}
            name={attribute.name}
            placeholder={attribute.placeholder}
            type={attribute.type}
            className='regular-style'
            value={value}
            onChange={onChange}
            onBlur={onBlur}
            />        
        </div>
    )
};

export default Input;