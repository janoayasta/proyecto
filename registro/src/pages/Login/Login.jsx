import React, {useState} from 'react';
import './Login.css';
import Title from './components/Title/Title';
import Label from './components/Label/Label';
import { Formik, Form, Field, ErrorMessage } from 'formik';

const Login = () => {

    const [formularioEnviado, setFormularioEnviado] = useState();

    return(
        <>
         <Formik
            initialValues={{
                user: '',
                password: ''
            }}
            validate={(valores)=>{
                let errores = {};
                //Validación nombre
                if(!valores.user){
                    errores.user= 'Usuario no valido'
                }else if(!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(valores.user)){
                    //Expresion regular
                    errores.user= 'Usuario solo admite letras'
                }
                //Validación password
                if(!valores.password){
                    errores.password='Contraseña no valida'
                }else if(!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(valores.password)){
                    //Expresion regular
                    errores.password= 'Usuario solo admite letras'
                }
                return errores;
            }}
            onSubmit={(valores, {resetForm}) => {
                resetForm();
                console.log('Formulario enviado');
                setFormularioEnviado(true);
                setTimeout(() => setFormularioEnviado(false),5000);
            }}
         >
            { ({errors} ) => (
            <Form className='formulario' >
                <div>
                    <Title text='Login'/>
                    <Label text='Usuario'/>
                    <Field
                        id='user'
                        name='user'
                        placeholder='Ingresar usuario'
                        type='text'
                        className='regular-style'
                    />
                    <ErrorMessage  name='user' component={() => (
                        <div className='error'>{errors.user}</div>
                    )}/>
                    <Label text='Contraseña'/>
                    <Field
                        id='password'
                        name='password'
                        placeholder='Ingresar contraseña'
                        type='password'
                        className='regular-style'  
                    />
                    <ErrorMessage  name='password' component={() => (
                        <div className='error'>{errors.password}</div>
                    )}/>
                    <button className='buttom-container' type='submit'>
                        Ingresar
                    </button>
                    {formularioEnviado && <p className='exito'>Formulario enviado con exito!</p>}
                </div>
            </Form>
        )}
            {/* { ({handleSubmit, values, handleChange, handleBlur, errors, touched} ) => (
            <form className='formulario' onSubmit={handleSubmit}>
                <div>
                    <Title text='Login'/>
                    <Label text='Usuario'/>
                    <input
                        id='user'
                        name='user'
                        placeholder='Ingresar usuario'
                        type='text'
                        className='regular-style'
                        value={values.user}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                    { touched.user && errors.user && <div className='error'>{errors.user}</div>}
                    <Label text='Contraseña'/>
                    <input
                        id='password'
                        name='password'
                        placeholder='Ingresar contraseña'
                        type='password'
                        className='regular-style'
                        value={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                    { touched.password && errors.password && <div className='error'>{errors.password}</div>}
                    <button className='buttom-container' type='submit'>
                        Ingresar
                    </button>
                    {formularioEnviado && <p className='exito'>Formulario enviado con exito!</p>}
                </div>
            </form> )} */}
        </Formik>
        </>
    )
};

export default Login;